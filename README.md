1. Install minikube - https://github.com/kubernetes/minikube
1. Create a new project
1. Disable shared runners for this project
1. Configure GitLab CI Runner (https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/howto/runner.md) for this project. Use Docker executor and privileged mode (https://docs.gitlab.com/ce/ci/docker/using_docker_build.html#use-docker-in-docker-executor)
1. Set following project variables:
  - `REGISTRY_USER` - your user name on https://hub.docker.com
  - `REGISTRY_PASSWORD` - your password on https://hub.docker.com
1. Configure Kubernetes service:
  - API URL - run `minikube ip` to get the IP of your cluster. The value for this variable is: `https://IP:8443`, probably `https://192.168.99.100:8443`
  - token - run `minikube dashboard`, it will open your browser. Go to Config -> Secrets -> a default token and you will find the token at the bottom
  - custom CA bundle - it is just above the token in the dashboard
1. Copy contents of https://gitlab.com/ayufan/kubernetes-deploy/tree/master/examples/rails-app to a fresh repository.
1. Change `image` in `.gitlab-ci.yml` to `adamniedzielski/gitlab-kubernetes-deploy`
1. Push the repository
1. Inspect build status
