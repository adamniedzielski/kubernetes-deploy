{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 24 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 24 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
*/}}
{{- define "fullname" -}}
{{- $name := default .Chart.Name .Values.nameOverride | trunc 24 | trimSuffix "-" -}}
{{- printf "%s-%s" .Release.Name $name -}}
{{- end -}}

{{/*
Create a database URL that can be accessed by application
*/}}
{{- define "database_url" -}}
{{- $user := .Values.mysql.mysqlUser -}}
{{- $password := .Values.mysql.mysqlPassword -}}
{{- $name := default .Chart.Name .Values.mysql.nameOverride | trunc 24 | trimSuffix "-" -}}
{{- $name := printf "%s-%s" .Release.Name $name -}}
{{- $port := 3306 -}}
{{- $database := .Values.mysql.mysqlDatabase -}}
{{- printf "mysql2://%s:%s@%s-mysql:%d/%s" $user $password $name $port $database -}}
{{- end -}}
