REPO := adamniedzielski/gitlab-kubernetes-deploy

build_and_push:
		docker build -t $(REPO) .
		docker push $(REPO)

build_test:
		./test build build $(PWD)/examples/rails-app/

deploy_test:
		./test deploy review $(PWD)/examples/rails-app/

.PHONY: build_and_push build_test
